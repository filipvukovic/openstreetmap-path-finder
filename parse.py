'''
Parse.py takes exported data from Overpass turbo api in json format and makes 
a networkx graph. It uses Haversine's formula to calculate real life distances between the nodes
and uses them as weights in the graph.
It outputs two files: 
    list_of_nods.json 
    osijek_graph.json
'''

import json
import networkx as nx
from math import radians, cos, sin, asin, sqrt



def haversine(lon1, lat1, lon2, lat2):
    lon1, lat1, lon2, lat2 = map(radians, [lon1, lat1, lon2, lat2])
   
    dlon = lon2 - lon1 
    dlat = lat2 - lat1 
    a = sin(dlat/2)**2 + cos(lat1) * cos(lat2) * sin(dlon/2)**2
    c = 2 * asin(sqrt(a)) 
    r = 6371 
    return c * r

if __name__ == '__main__':
    with open('export_osijek.json', 'r', encoding='utf8') as file:
        data = json.load(file)
        
    G = nx.Graph()      
    list_of_ways = []    
    list_of_nods = []
    for element in data['elements']:
        if element['type'] == 'way':
            list_of_ways.append(element['nodes'])
        if element['type'] == 'node':
            temp = element
            del temp['type']
            if 'tags' in temp.keys():
                del temp['tags']
            list_of_nods.append(temp)
    for node in list_of_nods: 
        G.add_node(node['id'])
        for way in list_of_ways:
            for i in range(len(way)):
                if node['id'] == way[i]:
                    way[i] = node                
    
    for way in list_of_ways:
            for i in range(len(way)-1):
                distance = haversine(way[i]['lon'],way[i]['lat'],way[i+1]['lon'],way[i+1]['lat'])
                G.add_edge(way[i]['id'], way[i+1]['id'], weight=distance)
                
    json_list = json.dumps(list_of_nods)             
    with open("list_of_nods.json", "w") as outfile: 
        outfile.write(json_list) 
    graph = nx.readwrite.json_graph.node_link_data(G)
    with open("osijek_graph.json","w") as outfile:
        outfile.write(json.dumps(graph))
    
# OpenStreetMap Path Finder

A small app written in Python to demostrate Dijsktra and A* shortest path algoritms on graphs in road navigation.
Overpass_query.txt containts overpass turbo query for filtring OpenStreetMaps data to get a road map.
Export_osijek.json containts data from exporting a map of the city of Osijek using overpass_query.

'''
Uses previosly exported and parsed data from OpeenStreetmaps to make a small app that demostrates usage 
and difference between Dijkstra and A* algorithms.

'''

import json
import networkx as nx
import matplotlib.pyplot as plt
import mplleaflet
from math import inf
import tkinter as tk
import webbrowser


import heapq
from itertools import count
from math import radians, cos, sin, asin, sqrt

def haversine(lon1, lat1, lon2, lat2):
    lon1, lat1, lon2, lat2 = map(radians, [lon1, lat1, lon2, lat2])
    dlon = lon2 - lon1 
    dlat = lat2 - lat1 
    a = sin(dlat/2)**2 + cos(lat1) * cos(lat2) * sin(dlon/2)**2
    c = 2 * asin(sqrt(a)) 
    r = 6371
    return c * r

def heuristic(G,neighbor,target):
    '''
    Heuristic function used in A* star algorithm, uses Haversine formula
    '''
    return haversine(G.node[neighbor]['lon'],G.node[neighbor]['lat'],G.node[target]['lon'],G.node[target]['lat'])
def astar_path(G, source, target, mode, weight='weight'):
    '''
    A* algorithm implementation on a networkx graph
    '''
    if source not in G or target not in G:
        msg = 'Either source {} or target {} is not in G'
        raise nx.NodeNotFound(msg.format(source, target))


    push = heapq.heappush
    pop = heapq.heappop


    c = count()
    queue = [(0, next(c), source, 0, None)]
    enqueued = {}
    explored = {}

    while queue:
        _, __, curnode, dist, parent = pop(queue)

        if curnode == target:
            path = [curnode]
            node = parent
            while node is not None:
                path.append(node)
                node = explored[node]
            path.reverse()
            if mode == 0:
                return path
            if mode == 1:
                return [path, list(explored)]

        if curnode in explored:
            if explored[curnode] is None:
                continue

            qcost, h = enqueued[curnode]
            if qcost < dist:
                continue

        explored[curnode] = parent

        for neighbor, w in G[curnode].items():
            ncost = dist + w.get(weight, 1)
            if neighbor in enqueued:
                qcost, h = enqueued[neighbor]
                if qcost <= ncost:
                    continue
            else:
                h = heuristic(G,neighbor, target)
            enqueued[neighbor] = ncost, h
            push(queue, (ncost + h, next(c), neighbor, ncost, curnode))

    raise nx.NetworkXNoPath("Node %s not reachable from %s" % (target, source))



def dijkstra(G,start,end,mode):
    '''
    Dijkstra algorithm implementation on a networkx graph
    '''
    if start not in G or end not in G:
        msg = 'Either source {} or target {} is not in G'
        raise nx.NodeNotFound(msg.format(start, end))
        
    visited = []
    minHeap = []
    heapq.heappush(minHeap, [0, start])
    prev = {nodes: None for nodes in list(G)}
    distance = {nodes: inf for nodes in list(G)}
    distance[start] = 0
    current = start
    while minHeap:
        current_weight, current = heapq.heappop(minHeap)
        visited.append(current)
        for neighbor in G.neighbors(current):
            if current_weight + G.get_edge_data(current,neighbor)['weight'] < distance[neighbor]:
                distance[neighbor] = current_weight + G.get_edge_data(current,neighbor)['weight']  
                prev[neighbor] = current
                heapq.heappush(minHeap,(distance[neighbor],neighbor))
        if end in visited: 
            path = []
            path.append(end)
            it = end
            while prev[it] != None:
                path.append(prev[it])
                it = prev[it]
            path.reverse()    
            if mode == 0:
                return path
            if mode == 1:
                return [path, visited]   
    raise nx.NetworkXNoPath("Node %s not reachable from %s" % (start, end))    

def find_path(node1, node2, show_nodes, method):
    '''
    Function used to link the gui part of the program with the implemented algorithms
    Plotes the results to matplotlib which then uses mplleaflet module to visualy display it
    '''
    if method == 'Dijkstra':
        if show_nodes == 0:
            path = dijkstra(G,node1,node2,0)
            longitude = []
            latitude = []
            for node in path:
                longitude.append(G.node[node]['lon'])
                latitude.append(G.node[node]['lat'])    
            plt.plot(longitude[0],latitude[0],'gs')    
            plt.plot(longitude[len(longitude)-1],latitude[len(latitude)-1],'rs')   
            plt.plot(longitude, latitude, 'b', linewidth=3.5) 
            mplleaflet.show()    
        if show_nodes == 1:
            [path,visited] = dijkstra(G,node1,node2,1)
            longitude = []
            latitude = []
            longitude_visited = []
            latitude_visited = []
            for node in path:
                longitude.append(G.node[node]['lon'])
                latitude.append(G.node[node]['lat'])
            for i in range(1,len(visited)-1):
                longitude_visited.append(G.node[visited[i]]['lon'])
                latitude_visited.append(G.node[visited[i]]['lat'])
            plt.plot(longitude_visited, latitude_visited,'m.')    
            plt.plot(longitude[0],latitude[0],'gs')    
            plt.plot(longitude[len(longitude)-1],latitude[len(latitude)-1],'rs')   
            plt.plot(longitude, latitude, 'b', linewidth=3.5) 
            mplleaflet.show()
    if method == 'A*':
        if show_nodes == 0:
            path = astar_path(G,node1,node2,0,weight='weight') 
            longitude = []
            latitude = []   
            for node in path:
                    longitude.append(G.node[node]['lon'])
                    latitude.append(G.node[node]['lat'])    
            plt.plot(longitude[0],latitude[0],'gs')    
            plt.plot(longitude[len(longitude)-1],latitude[len(latitude)-1],'rs')   
            plt.plot(longitude, latitude, 'b', linewidth=3.5)  
            mplleaflet.show()       
        if show_nodes == 1:
            [path,visited] = astar_path(G,node1,node2, 1, weight='weight')
            longitude = []
            latitude = []
            longitude_visited = []
            latitude_visited = []
            for node in path:
                longitude.append(G.node[node]['lon'])
                latitude.append(G.node[node]['lat'])
            for i in range(1,len(visited)-1):
                longitude_visited.append(G.node[visited[i]]['lon'])
                latitude_visited.append(G.node[visited[i]]['lat'])
            plt.plot(longitude_visited, latitude_visited,'m.')    
            plt.plot(longitude[0],latitude[0],'gs')    
            plt.plot(longitude[len(longitude)-1],latitude[len(latitude)-1],'rs')   
            plt.plot(longitude, latitude, 'b', linewidth=3.5)  
            mplleaflet.show()
            
class GUI:
    '''
    A small GUI made in tkinter to simplify usage
    '''
    def __init__(self,root):
        self.start = 0
        self.end = 0
        self.show_nodes = 0
        self.topFrame = tk.Frame(root)
        self.topFrame.pack()
        self.bottomFrame = tk.Frame(root)
        self.bottomFrame.pack(side=tk.BOTTOM)
    
        self.label_start = tk.Label(self.topFrame, text='Starting Point')
        self.label_start_id = tk.Label(self.topFrame, text='ID: ')
        self.entry_start = tk.Entry(self.topFrame)
        
        self.label_start.grid(row = 0, column = 1)
        self.label_start_id.grid(row = 1, column = 0, sticky = tk.E)
        self.entry_start.grid(row = 1, column = 1)
        
        self.label_end = tk.Label(self.topFrame, text='End Point')
        self.label_end_id = tk.Label(self.topFrame, text='ID: ')
        self.entry_end = tk.Entry(self.topFrame)
        
        self.label_end.grid(row = 2, column = 1)
        self.label_end_id.grid(row = 3, column = 0, sticky = tk.E)
        self.entry_end.grid(row = 3, column = 1)
 
        
        self.var = tk.StringVar(root)
        self.var.set("Dijkstra")
        self.option= tk.OptionMenu(self.topFrame, self.var, 'Dijkstra', 'A*')
        self.option.grid(row = 1, column = 4)
        self.label_select = tk.Label(self.topFrame, text='Select method:')
        self.label_select.grid(row=0, column = 4)
        
        self.var2 = tk.IntVar()
        self.check_nodes = tk.Checkbutton(self.topFrame, text = 'Show visited nodes',variable= self.var2, onvalue=1, offvalue=0, command=self.check_nodes)
        self.check_nodes.grid(row = 2, column = 4, sticky = tk.W)
        self.mapButton = tk.Button(self.bottomFrame, text = 'Open Map')
        self.mapButton.bind('<Button-1>', self.open_map)
        self.mapButton.pack()
        self.startButton = tk.Button(self.bottomFrame, text = 'Find Path')
        self.startButton.bind('<Button-1>', self.start_button)
        self.startButton.pack()
        
    def open_map(self,event):
        webbrowser.open('map.html')
        
    def check_nodes(self):
        if self.var2.get() == 1:
            self.show_nodes = 1
        else:
            self.show_nodes = 0
    def start_button(self,event):
        self.start = int(self.entry_start.get())  
        self.end = int(self.entry_end.get())  
        find_path(self.start,self.end,self.show_nodes,self.var.get())

            
if __name__ == '__main__':
    with open("osijek_graph.json", 'r', encoding='utf8') as file:
        graph = json.load(file)
    G = nx.readwrite.json_graph.node_link_graph(graph)
    
    root = tk.Tk()
    root.title('OpenStreetMaps Path Finder')
    interface = GUI(root)
    
    root.mainloop()